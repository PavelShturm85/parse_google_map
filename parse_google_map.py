import threading
from pymongo import MongoClient
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time


settings = {
    "chrome_driver": "./chromedriver",
    "search_word": "Бетон",
    "latitude_begin": 69.50,
    "latitude_end": 39.50,
    "longitude_begin": 30.30,
    "longitude_end": 180.00,
    "company_list": "section-result",
    "btn_back": ".section-back-to-list-button.blue-link.noprint",
    "title": "section-hero-header-title",
    "address": 'div[data-section-id="ad"] .section-info-text',
    "url": 'div[data-section-id="ap"] .section-info-text',
    "phone": 'div[data-section-id="pn0"] .section-info-text',
    "btn_pagination_next": "n7lv7yjyC35__section-pagination-button-next",
    "no_result": "section-no-result-title",
    "zoom_out": "widget-zoom-out",
    "btn_search_this_area": "widget-search-this-area-inner",
    "widget_mylocation": "widget-mylocation",
}

NO_SAVE_LIST = ('Узбекистан', 'Казахстан', 'Украина', 'Беларусь', 'Турция', 'Грузия', 'Азербайджан', 'Армения',
                'Киргизия', 'Туркменистан', 'Таджикистан', 'Китай', 'Монголия', 'Япония', 'Латвия', 'Литва',
                'Эстония', 'Финляндия', 'Норвегия', 'Україна', 'Румыния', 'Молдова')


class ParseGoogleMap():
    def __init__(self, search_word=settings["search_word"]):
        self.template_url = "https://www.google.ru/maps/search/{search_word}/@{latitude},{longitude},9z"
        self.search_word = search_word
        mongo = MongoClient('127.0.0.1', 27017, connect=False)
        db = mongo.companies_db
        self.company = db.google_companies
        self.lock = threading.Lock()
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        chrome = webdriver.Chrome(executable_path=settings['chrome_driver'], chrome_options=options)
        chrome.implicitly_wait(1)
        chrome.set_window_size(1920, 1080)
        chrome.maximize_window()
        self.chrome = chrome
        self.wait_element = WebDriverWait(self.chrome, 1)

    def __save_company(self, company):
        with self.lock:
            self.company.save(company)
            print("save: {}".format(company))

    def __check_company_detail(self, company_detail):
        with self.lock:
            is_in_base = self.company.find_one(company_detail)
            if is_in_base:
                print("организация уже в базе")
        return bool(is_in_base)

    def __check_no_save(self, address):

        for country in NO_SAVE_LIST:
            if country in address:
                print('страна организации в списке на исключение')
                return True

    def __move_and_click(self, element, pause_time):
        ActionChains(self.chrome).move_to_element(
            element).pause(pause_time).click().perform()

    def __get_company_detail(self):

        try:
            title = self.wait_element.until(EC.presence_of_element_located(
                (By.CLASS_NAME, settings["title"]))).text

            try:
                url = self.chrome.find_element_by_css_selector(
                    settings["url"]).text

            except:
                url = ''
            try:
                address = self.chrome.find_element_by_css_selector(
                    settings["address"]).text

            except:
                address = ''
            try:
                phone = self.chrome.find_element_by_css_selector(
                    settings["phone"]).text

            except:
                phone = ''

            company_details = {
                "title": title,
                "url": url,
                "address": address,
                "phone": phone,
            }
            return company_details
        except:
            return {}

    def __get_company_list_and_save(self):
        try:
            company_list = self.wait_element.until(
                EC.presence_of_all_elements_located((By.CLASS_NAME, settings['company_list'])))
        except:
            self.chrome.refresh()
            company_list = self.wait_element.until(
                EC.presence_of_all_elements_located((By.CLASS_NAME, settings['company_list'])))
        print(len(company_list))
        len_company_list = len(company_list)
        num = 0
        while num < len_company_list:
            self.__move_and_click(company_list[num], 3)
            num += 1
            company_detail = self.__get_company_detail()
            print(num, "зашли в детализацию")

            if company_detail:
                is_in_base = self.__check_company_detail(company_detail)
                address_company = company_detail.get('address')
                is_in_no_save_list = self.__check_no_save(address_company)
                if not is_in_base and not is_in_no_save_list:
                    self.__save_company(company_detail)
            try:
                btn_back = self.wait_element.until(
                    EC.presence_of_element_located((By.CSS_SELECTOR, settings['btn_back'])))
            except:
                self.chrome.refresh()
                btn_back = self.wait_element.until(
                    EC.presence_of_element_located((By.CSS_SELECTOR, settings['btn_back'])))
            print("вернулись к списку")
            self.__move_and_click(btn_back, 2)
            try:
                company_list = self.wait_element.until(
                    EC.presence_of_all_elements_located((By.CLASS_NAME, settings['company_list'])))
            except:
                self.chrome.refresh()
                company_list = self.wait_element.until(
                    EC.presence_of_all_elements_located((By.CLASS_NAME, settings['company_list'])))
        print("обход компаний в координатах закончен")

    def __pagination_on_page(self):
        try:
            btn_pagination_next = self.wait_element.until(
                EC.element_to_be_clickable((By.ID, settings['btn_pagination_next'])))
            while btn_pagination_next:
                self.__move_and_click(btn_pagination_next, 2)
                self.__get_company_list_and_save()
                try:
                    btn_pagination_next = self.wait_element.until(
                        EC.element_to_be_clickable((By.ID, settings['btn_pagination_next'])))
                    print("следующая страница с компаниями")
                except:
                    break
        except:
            pass

    def __push_zoom_and_btn_search(self):
        try:
            zoom_out = WebDriverWait(self.chrome, 5).until(
                EC.element_to_be_clickable((By.ID, settings['zoom_out'])))
            self.__move_and_click(zoom_out, 1)
            btn_search_this_area = WebDriverWait(self.chrome, 3).until(
                EC.element_to_be_clickable((By.CLASS_NAME, settings['btn_search_this_area'])))
            self.__move_and_click(btn_search_this_area, 1)
        except:
            self.chrome.refresh()
            print("перезагружаем страницу")
            zoom_out = WebDriverWait(self.chrome, 5).until(
                EC.element_to_be_clickable((By.ID, settings['zoom_out'])))
            self.__move_and_click(zoom_out, 1)
            btn_search_this_area = WebDriverWait(self.chrome, 3).until(
                EC.element_to_be_clickable((By.CLASS_NAME, settings['btn_search_this_area'])))
            self.__move_and_click(btn_search_this_area, 1)

    def _check_page_by_company(self, num, latitude_begin=77.00, latitude_end=41.00, longitude_begin=27.00, longitude_end=180.00):
        def next_step(latitude, longitude):
            if longitude <= longitude_end:
                longitude += 7.0
            elif longitude >= longitude_end:
                longitude = longitude_begin
                if latitude > 70.0:
                    latitude -= 1.7
                elif 67.0 < latitude <= 69.0:
                    latitude -= 1.8
                elif 65.0 < latitude <= 67.0:
                    latitude -= 1.9
                elif 63.0 < latitude <= 65.0:
                    latitude -= 2.0
                elif 60.0 < latitude <= 63.0:
                    latitude -= 2.1
                elif 58.0 < latitude <= 60.0:
                    latitude -= 2.2
                elif 55.0 < latitude <= 58.0:
                    latitude -= 2.3
                elif 53.0 < latitude <= 55.0:
                    latitude -= 2.4
                elif 50.0 < latitude <= 53.0:
                    latitude -= 2.5
                elif 47.0 < latitude <= 50.0:
                    latitude -= 2.6
                elif 44.0 < latitude <= 47.0:
                    latitude -= 2.7
                else:
                    latitude -= 2.8
            return latitude, longitude

        latitude, longitude = (latitude_begin, longitude_begin)
        with self.lock:
            is_last_point = self.company.find(
                {"last_point_{}".format(num): True})
        if is_last_point:
            for point in is_last_point:
                latitude, longitude = point.get("coordinate")
        if longitude > longitude_end:
            latitude, longitude = next_step(latitude, longitude)
        if latitude <= latitude_end:
            self.chrome.quit()
            print('Поток-{}: Сбор информации окончен'.format(num))
        while latitude > latitude_end:
            query = {"last_point_{}".format(num): True}
            data = {"last_point_{}".format(num): True, "coordinate": [
                latitude, longitude]}
            with self.lock:
                self.company.update(query, data, upsert=True)
            url = self.template_url.format(longitude=str(longitude), latitude=str(
                latitude), search_word=str(self.search_word))
            print('Поток-{}: {}'.format(num, url))
            try:
                self.chrome.get(url)
            except:
                self.chrome.refresh()
            print("Поток-{}: открываем страницу".format(num))

            try:
                self.__push_zoom_and_btn_search()
            except:
                pass

            try:
                widget_mylocation = WebDriverWait(self.chrome, 5).until(
                    EC.presence_of_element_located((By.ID, settings['widget_mylocation'])))
                try:
                    no_result = WebDriverWait(self.chrome, 3).until(
                        EC.presence_of_element_located((By.CLASS_NAME, settings['no_result'])))
                    latitude, longitude = next_step(latitude, longitude)
                    print(
                        "Поток-{}: нет данных по запросу '{}' на карте.".format(num, self.search_word))
                    if latitude <= latitude_end:
                        self.chrome.quit()
                        print('Поток-{}: Сбор информации окончен'.format(num))
                    continue
                except:
                    pass
            except:
                try:
                    self.chrome.refresh()
                    self.__push_zoom_and_btn_search()
                    no_result = WebDriverWait(self.chrome, 3).until(
                        EC.presence_of_element_located((By.CLASS_NAME, settings['no_result'])))
                    latitude, longitude = next_step(latitude, longitude)
                    print(
                        "Поток-{}: нет данных по запросу '{}' на карте.".format(num, self.search_word))
                    if latitude <= latitude_end:
                        self.chrome.quit()
                        print('Поток-{}: Сбор информации окончен'.format(num))
                    continue
                except:
                    pass

            try:
                self.__get_company_list_and_save()
                print("Поток-{}: нашли элементы".format(num))
                self.__pagination_on_page()
                latitude, longitude = next_step(latitude, longitude)
                if latitude <= latitude_end:
                    self.chrome.quit()
                    print('Поток-{}: Сбор информации окончен'.format(num))
                continue
            except:
                company_detail = self.__get_company_detail()
                print("Поток-{}: получили данные из детализации".format(num))

                if company_detail:
                    is_in_base = self.__check_company_detail(company_detail)
                    address_company = company_detail.get('address')
                    is_in_no_save_list = self.__check_no_save(address_company)
                    if not is_in_base and not is_in_no_save_list:
                        self.__save_company(company_detail)
                    else:
                        print("Поток-{}: компания уже в базе".format(num))
                    latitude, longitude = next_step(latitude, longitude)
                    if latitude <= latitude_end:
                        self.chrome.quit()
                        print('Поток-{}: Сбор информации окончен'.format(num))
                    continue


def __select_number_threads():
    import multiprocessing
    cpu_count = int(multiprocessing.cpu_count())
    if cpu_count > 4:
        threads = cpu_count - 2
    elif 1 < cpu_count <= 4:
        threads = cpu_count - 1
    else:
        threads = cpu_count
    return threads


def get_targets_and_params():
    step = __select_number_threads()
    latitude_begin = settings["latitude_begin"]
    latitude_end = settings["latitude_end"]
    len_latitude = latitude_begin - latitude_end
    step_for_thread = len_latitude/step
    params = []
    target = []
    curr_latitude_begin = latitude_begin
    curr_latitude_end = latitude_begin - step_for_thread
    num = 1
    
    while curr_latitude_begin - step_for_thread > latitude_end and curr_latitude_end > latitude_end:
        google_parser = ParseGoogleMap()
        params.append((num, curr_latitude_begin, curr_latitude_end,
                       settings["longitude_begin"], settings["longitude_end"],))
        target.append(google_parser._check_page_by_company)
        num += 1
        curr_latitude_begin = curr_latitude_end
        curr_latitude_end = curr_latitude_begin - step_for_thread
    return zip(target, params)


def main():
    targets_and_params = get_targets_and_params()

    for target, param in targets_and_params:
        print(param)
        my_thread = threading.Thread(target=target, args=(*param,))
        my_thread.start()


if __name__ == "__main__":
    main()
